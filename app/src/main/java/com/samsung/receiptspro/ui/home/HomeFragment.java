package com.samsung.receiptspro.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.samsung.receiptspro.MainActivity;
import com.samsung.receiptspro.R;
import com.samsung.receiptspro.account.models.User;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.Receipt;
import com.samsung.receiptspro.network.models.ReceiptsList;
import com.samsung.receiptspro.network.models.ReceiveResult;
import com.samsung.receiptspro.receipts.ReceiptDisplayActivity;
import com.samsung.receiptspro.receipts.adapters.ReceiptsAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener, ReceiptsAdapter.OnReceiptListener {

    private HomeViewModel homeViewModel;
    private Context mContext;
    private RecyclerView recyclerView;
    private ReceiptsAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefresh;
//    private boolean isFirstUpdate = true;
//    private TextView mReceiptsDefault;
    private static Server client;
    private static User currentUser;
    private List<Receipt> mReceipts = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = root.findViewById(R.id.receipts_view);
        mContext = requireContext();
        mAdapter = new ReceiptsAdapter(mContext, mReceipts, this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        client = MainActivity.client;
        currentUser = MainActivity.currentUser;

        getReceipts();

        mSwipeRefresh = root.findViewById(R.id.home_swipe_refresh);
        mSwipeRefresh.setColorSchemeResources(
                R.color.purple,
                R.color.blue,
                R.color.green,
                R.color.orange
        );
        mSwipeRefresh.setOnRefreshListener(this);

//        mReceiptsDefault = root.findViewById(R.id.receipts_default);

        return root;
    }

    @Override
    public void onRefresh() {
        getReceipts();
    }

    private void getReceipts(){
        Call<ReceiptsList> call = client.getReceiptsRange(currentUser.getPhone(), "all");
        call.enqueue(new Callback<ReceiptsList>() {
            @Override
            public void onResponse(@NonNull Call<ReceiptsList> call, @NonNull Response<ReceiptsList> response) {
                if (response.isSuccessful()) {
                    receiptSuccess(Objects.requireNonNull(response.body()));
                } else {
                    receiptFailure();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReceiptsList> call, @NonNull Throwable t) {
                receiptError();
                t.printStackTrace();
            }
        });
    }

    private void receiptSuccess(ReceiptsList receipts) {
//        if (isFirstUpdate){
//            mReceiptsDefault
//            isFirstUpdate = false;
//        }
        mReceipts = receipts.getReceipts();
        mSwipeRefresh.setRefreshing(false);
        mAdapter = new ReceiptsAdapter(mContext, mReceipts, this);
        recyclerView.setAdapter(mAdapter);
    }

    private void receiptFailure() {
        mSwipeRefresh.setRefreshing(false);
        showToast(getString(R.string.user_not_found));
    }

    private void receiptError() {
        mSwipeRefresh.setRefreshing(false);
        showToast(getString(R.string.connection_error));
    }

    private void showToast(String text){
        Toast.makeText(
                mContext,
                text,
                Toast.LENGTH_LONG
        ).show();
    }

    @Override
    public void onReceiptClick(int position) {
        Intent intent = new Intent(mContext, ReceiptDisplayActivity.class);
        intent.putExtra("receipt", new ReceiveResult(mReceipts.get(position).getReceipt()));
        startActivity(intent);
    }
}