package com.samsung.receiptspro.ui.charts;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.samsung.receiptspro.R;
import com.samsung.receiptspro.charts.ChartManager;

public class ChartsFragment extends Fragment
        implements OnChartGestureListener, SwipeRefreshLayout.OnRefreshListener {

    private ChartsViewModel mViewModel;
    private Context mContext;
    private ChartManager mChartManager;
    private Spinner periods_spinner;
    private Spinner chart_type_spinner;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_charts, container, false);
        mContext = requireContext();

        mChartManager = new ChartManager(mContext, root);

        chart_type_spinner = root.findViewById(R.id.spinner_chart_types);
        ArrayAdapter<String> chart_adapter = new ArrayAdapter<>(mContext,
                R.layout.spinner_item,
                mContext.getResources().getStringArray(R.array.chart_types));
        chart_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        chart_type_spinner.setAdapter(chart_adapter);
        chart_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                mChartManager.changeChartType(selectedItemPosition);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        periods_spinner = root.findViewById(R.id.spinner_periods);
        ArrayAdapter<String> period_adapter = new ArrayAdapter<>(mContext,
                R.layout.spinner_item,
                mContext.getResources().getStringArray(R.array.period_types));
        period_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        periods_spinner.setAdapter(period_adapter);
        periods_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                mChartManager.getReceipts(selectedItemPosition);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        SwipeRefreshLayout swipeRefresh = root.findViewById(R.id.charts_swipe_refresh);
        swipeRefresh.setColorSchemeResources(
                R.color.purple,
                R.color.blue,
                R.color.green,
                R.color.orange
        );
        swipeRefresh.setOnRefreshListener(this);

        ListView chartsListView = root.findViewById(R.id.charts_list_view);
        chartsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int topRowVerticalPosition = chartsListView.getChildCount() == 0 ? 0 : chartsListView.getChildAt(0).getTop();
                swipeRefresh.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }
        });


        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ChartsViewModel.class);
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onRefresh() {
        mChartManager.getReceipts(periods_spinner.getSelectedItemPosition());
    }
}