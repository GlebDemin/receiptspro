package com.samsung.receiptspro.charts.items;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.ChartData;
import com.samsung.receiptspro.R;

public class BarChartItem extends ChartItem {

    private ViewHolder holder;
    private final int WHITE = Color.rgb(255, 255, 255);
    public BarChartItem(ChartData<?> cd, Context c) {
        super(cd);
    }

    @Override
    public int getItemType() {
        return TYPE_BARCHART;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, Context c) {
        if (convertView == null) {

            holder = new ViewHolder();

            convertView = LayoutInflater.from(c).inflate(
                    R.layout.list_item_barchart, null);
            holder.chart = convertView.findViewById(R.id.barChart);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.chart.getDescription().setEnabled(false);
        holder.chart.setDrawGridBackground(false);
        holder.chart.setDrawBarShadow(false);
        Legend legend = holder.chart.getLegend();
        legend.setTextColor(WHITE);
        holder.chart.getAxisLeft().setTextColor(WHITE);
        holder.chart.getAxisRight().setTextColor(WHITE);

        XAxis xAxis = holder.chart.getXAxis();
        xAxis.setTextColor(WHITE);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = holder.chart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setSpaceTop(20f);
        leftAxis.setAxisMinimum(0f);

        YAxis rightAxis = holder.chart.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setSpaceTop(20f);
        rightAxis.setAxisMinimum(0f);

        holder.chart.setData((BarData) mChartData);
        holder.chart.setFitBars(true);

        holder.chart.animateY(500);
        holder.chart.invalidate();
        return convertView;
    }

    @Override
    public void updateView() {
        holder.chart.invalidate();
    }

    @Override
    public void setData(ChartData<?> data) {
        mChartData = data;
        holder.chart.setData((BarData) mChartData);
    }

    @Override
    public int getChartType() {
        return TYPE_BARCHART;
    }

    private static class ViewHolder {
        BarChart chart;
    }
}