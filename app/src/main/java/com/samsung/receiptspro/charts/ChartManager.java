package com.samsung.receiptspro.charts;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.samsung.receiptspro.MainActivity;
import com.samsung.receiptspro.R;
import com.samsung.receiptspro.account.models.User;
import com.samsung.receiptspro.charts.adapters.ChartDataAdapter;
import com.samsung.receiptspro.charts.items.BarChartItem;
import com.samsung.receiptspro.charts.items.ChartItem;
import com.samsung.receiptspro.charts.items.PieChartItem;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.ReceiptsList;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChartManager {
    private static String TAG = "ChartManager";
    private static Server client;
    private static BarChart barChart;
    private String notBought;
    private static User currentUser;
    private static PieChart pieChart;
    private SwipeRefreshLayout mParentSwipeRefresh;
    private ArrayList<ChartItem> mCharts;
    private ChartDataAdapter mChartDataAdapter;
    private Context parentContext;
    private String[] periods;
    private String dataLoading;
    private static ArrayList<Integer> data = new ArrayList<>(6);
    private String[] headers;
    private Integer categoriesCount = 6;

    public ChartManager(Context parentContext, View v){
        this.parentContext = parentContext;

        ListView chartsListView = v.findViewById(R.id.charts_list_view);

        mCharts = new ArrayList<>();

        mChartDataAdapter = new ChartDataAdapter(parentContext.getApplicationContext(), mCharts);
        chartsListView.setAdapter(mChartDataAdapter);

        Resources res = parentContext.getResources();
        Configuration conf = res.getConfiguration();
        Locale savedLocale = conf.locale;
        conf.locale = new Locale("en");
        res.updateConfiguration(conf, null);

        periods = res.getStringArray(R.array.period_types);

        conf.locale = savedLocale;
        res.updateConfiguration(conf, null);

        headers = res.getStringArray(R.array.chart_headers);
        notBought = res.getString(R.string.not_bought);
        dataLoading = res.getString(R.string.data_loading);
        client = MainActivity.client;
        currentUser = MainActivity.currentUser;
        mParentSwipeRefresh = v.findViewById(R.id.charts_swipe_refresh);
        initData();
    }

    public void changeChartType(int type){
        ChartItem item;
        switch (type) {
            case 0: {
                BarData barData = updateBarData();
                item = createBarChart(barData);
                break;
            }
            case 1: {
                PieData pieData = updatePieData();
                item = createPieChart(pieData);
                break;
            }
//            case 2: {
//                int a = 2;
//                break;
//            }
            default:
                BarData barData = updateBarData();
                item = createBarChart(barData);
        }
        if(mCharts.size() != 0){
            mCharts.set(0, item);
        }
        else{
            mCharts.add(item);
        }
        mChartDataAdapter.notifyDataSetChanged();
    }

    private BarData updateBarData(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < categoriesCount; ++i) {
            entries.add(new BarEntry(i, data.get(i)));
        }
        BarDataSet barDataSet = new BarDataSet(entries, stringJoin(" ", headers));
        barDataSet.setStackLabels(headers);
        barDataSet.setColors(MyOwnColors.JOYFUL_COLORS);
        barDataSet.setValueTextColor(Color.rgb(255, 255, 255));
        return new BarData(barDataSet);
    }

    private BarChartItem createBarChart(BarData barData){
        return new BarChartItem(barData, parentContext);
    }

    private PieData updatePieData(){
        ArrayList<PieEntry> entries = new ArrayList<>();
        for(int i = 0; i < categoriesCount; ++i) {
            if (data.get(i) != 0){
                entries.add(new PieEntry(data.get(i), headers[i]));
            }
        }
        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setColors(MyOwnColors.JOYFUL_COLORS);
        pieDataSet.setValueTextColor(Color.rgb(255, 255, 255));
        return new PieData(pieDataSet);
    }

    private PieChartItem createPieChart(PieData pieData){
        return new PieChartItem(pieData, parentContext);
    }

    private String stringJoin(String delimiter, String[] arr){
        StringBuilder result = new StringBuilder();
        for (String elem: arr) {
            result.append(elem).append(delimiter);
        }
        return result.toString();
    }

    private void initData() {
        for (int i = 0; i < categoriesCount; ++i) {
            data.add(0);
        }
    }

    private static void updArr() {
        for (int i = 0; i < 6; ++i) {
            data.set(i, 0);
        }
    }

    public void getReceipts(int periodIndex) {
        Call<ReceiptsList> call = client.getReceiptsRange(currentUser.getPhone(), periods[periodIndex].toLowerCase());
        call.enqueue(new Callback<ReceiptsList>() {
            @Override
            public void onResponse(@NonNull Call<ReceiptsList> call, @NonNull Response<ReceiptsList> response) {
                if (response.isSuccessful()) {
                    receiveSuccess(response.body());
                } else {
                    receiveFailure();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReceiptsList> call, @NonNull Throwable t) {
                receiveError();
                t.printStackTrace();
            }
        });
    }

    private void receiveSuccess(ReceiptsList receipt) {
        updateData(receipt);
        mParentSwipeRefresh.setRefreshing(false);
        ChartItem chart = mCharts.get(0);
        switch (chart.getChartType()){
            case 0:
                chart.setData(updateBarData());
                break;
            case 2:
                chart.setData(updatePieData());
                break;
        }
        chart.updateView();
//        barChart.setEnabled(true);
//        barChart.setVisibility(VISIBLE);
    }

    private  void receiveFailure() {
        showToast(parentContext.getString(R.string.user_not_found));
        mParentSwipeRefresh.setRefreshing(false);
    }

    private void receiveError() {
        showToast(parentContext.getString(R.string.connection_error));
        mParentSwipeRefresh.setRefreshing(false);
    }

    private void updateData(ReceiptsList receipt){
        updArr();
        for (int i = 0; i < receipt.getReceipts().size(); ++i) {
            for (int j = 0; j < receipt.getReceipts().get(i).getReceipt().getProducts().size(); ++j) {
                String cur = receipt.getReceipts().get(i).getReceipt().getProducts().get(j).getCategory();
                int res = getIdx(cur);
                Log.d(TAG, "receiptSuccess: " + cur + " " + res);
                data.set(res, data.get(res) + 1);
            }
        }
    }

    public static final int getIdx(String res) {
        switch (res) {
            case "Продукты":
                return 0;
            case "Алкогольная продукция":
                return 1;
            case "Безалкогольные напитки":
                return 2;
            case "Быт.Химия":
                return 3;
            case "Товары для дома":
                return 4;
            case "Средства личной гигиены":
                return 5;
            default:
                return -1;
        }
    }

    private void showToast(String text){
        Toast.makeText(
                parentContext,
                text,
                Toast.LENGTH_LONG
        ).show();
    }

}