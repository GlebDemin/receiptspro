package com.samsung.receiptspro.charts;

import android.graphics.Color;

import com.github.mikephil.charting.utils.ColorTemplate;

final class MyOwnColors extends ColorTemplate {
    static final int[] JOYFUL_COLORS = {android.graphics.Color.rgb(217, 80, 138), Color.rgb(254, 149, 7), Color.rgb(254, 247, 120),
            Color.rgb(106, 167, 134), Color.rgb(53, 194, 209), Color.rgb(228, 47, 228)};
}