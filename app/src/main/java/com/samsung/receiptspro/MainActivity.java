package com.samsung.receiptspro;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.samsung.receiptspro.account.AccountManager;
import com.samsung.receiptspro.account.models.User;
import com.samsung.receiptspro.auth.AuthActivity;
import com.samsung.receiptspro.network.BaseRequestMaker;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.ReceiveResult;
import com.samsung.receiptspro.receipts.ReceiptDisplayActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    private ProgressDialog mProgressDialog;
    public static Server client;

    private AccountManager accountManager;
    public static User currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        final Activity activity = this;
        fab.setOnClickListener(v -> {
            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
            integrator.setPrompt(getString(R.string.qr_scanner_prompt));
            integrator.initiateScan();
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_charts)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        this.accountManager = new AccountManager(
                getSharedPreferences(
                        AccountManager.getPrefsName(),
                        Context.MODE_PRIVATE
                )
        );
        currentUser = this.accountManager.getCurrentUser();
        Log.i("User", currentUser.toString());
        client = new BaseRequestMaker().getClient();
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setTitle(getString(R.string.receiving_receipt));
        this.mProgressDialog.setIndeterminate(false);
        this.mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.mProgressDialog.setCancelable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        TextView userEmail = findViewById(R.id.userEmailView);
        userEmail.setText(currentUser.getEmail());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            this.accountManager.logout();
            Intent intent = new Intent(this, AuthActivity.class);
            startActivity(intent);
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, getString(R.string.cancel_scan), Toast.LENGTH_LONG).show();
            } else {
                getReceipt(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getReceipt(String scanResult) {
        Call<ReceiveResult> call = client.receive(currentUser.getPhone(), currentUser.getPassword(), scanResult);

        this.mProgressDialog.show();

        call.enqueue(new Callback<ReceiveResult>() {
            @Override
            public void onResponse(@NonNull Call<ReceiveResult> call, @NonNull Response<ReceiveResult> response) {
                if (response.isSuccessful()) {
                    receiveSuccess(response.body());
                } else {
                    receiveFailure();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReceiveResult> call, @NonNull Throwable t) {
                Log.e("Error", Objects.requireNonNull(t.getMessage()));
                receiveError();
            }
        });
    }

    private void showToast(String text) {
        Toast.makeText(
                this,
                text,
                Toast.LENGTH_LONG
        ).show();
    }

    private void AfterResponse(String text) {
        showToast(text);
        this.mProgressDialog.cancel();
    }

    private void receiveSuccess(ReceiveResult receipt) {
        AfterResponse(getString(R.string.receiving_success));
        Intent intent = new Intent(this, ReceiptDisplayActivity.class);
        intent.putExtra("receipt", receipt);
        startActivity(intent);
    }

    private void receiveFailure() {
        AfterResponse(String.format("%s!", getString(R.string.user_not_found)));
    }

    private void receiveError() {
        AfterResponse(String.format("%s!", getString(R.string.connection_error)));
    }
}