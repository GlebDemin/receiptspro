package com.samsung.receiptspro.receipts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samsung.receiptspro.R;
import com.samsung.receiptspro.network.models.Receipt;

import java.util.List;

public class ReceiptsAdapter extends RecyclerView.Adapter<ReceiptsAdapter.ViewHolder> {

    private List<Receipt> mReceipts;
    private Context mContext;
    private OnReceiptListener mReceiptListener;

    public ReceiptsAdapter(Context context, List<Receipt> receipts, OnReceiptListener receiptListener) {
        mContext = context;
        mReceipts = receipts;
        mReceiptListener = receiptListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_receipt_item, parent, false);
        return new ViewHolder(view, mReceiptListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceiptsAdapter.ViewHolder holder, int position) {
        holder.receipt.setText(getReceiptName(position));
    }

    @Override
    public int getItemCount() {
        return mReceipts.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView receipt;
        LinearLayout parentLayout;
        OnReceiptListener receiptListener;

        ViewHolder(View itemView, OnReceiptListener receiptListener) {
            super(itemView);
            receipt = itemView.findViewById(R.id.receipt);
            parentLayout = itemView.findViewById(R.id.parent_layout_receipt);
            this.receiptListener = receiptListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            receiptListener.onReceiptClick(getAdapterPosition());
        }
    }

    private String getReceiptName(int position){
        Receipt receipt = mReceipts.get(position);
        return mContext.getString(R.string.receipt_name) + receipt.getReceipt().getDateTime();
    }

    public interface OnReceiptListener{

        void onReceiptClick(int position);
    }

}