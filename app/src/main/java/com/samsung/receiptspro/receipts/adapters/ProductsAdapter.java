package com.samsung.receiptspro.receipts.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.samsung.receiptspro.R;
import com.samsung.receiptspro.charts.ChartManager;
import com.samsung.receiptspro.network.models.Product;

import org.w3c.dom.Text;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private static final String TAG = "ProductsAdapter";
    private List<Product> mProducts;
    private Context mContext;
    private String product_name;
    private String product_price;
    private String currency;
    private String category_name;
    private String[] headers;

    public ProductsAdapter(Context context, List<Product> products) {
        for (Product product : products) {
            Log.i(TAG, product.toString());
        }
        mContext = context;
        Resources res = mContext.getResources();
        headers = res.getStringArray(R.array.chart_headers);
        mProducts = products;
        product_price = mContext.getString(R.string.product_price);
        product_name = mContext.getString(R.string.product_name);
        category_name = mContext.getString(R.string.category);
        currency = mContext.getString(R.string.currency);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_product_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.product.setMovementMethod(new ScrollingMovementMethod());
        holder.product.setText((product_name + getName(mProducts.get(position).getName())));
        holder.price.setText((product_price + getPrice(mProducts.get(position).getPrice()) + " " + currency));
        holder.category.setText((category_name + headers[ChartManager.getIdx(mProducts.get(position).getCategory())]));
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView price;
        TextView product;
        TextView category;
        LinearLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.product_price);
            category = itemView.findViewById(R.id.product_category);
            product = itemView.findViewById(R.id.product_name);
            product.setHorizontallyScrolling(true);
            product.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            product.setSelected(true);
            parentLayout = itemView.findViewById(R.id.parent_layout_product);
        }
    }

    private String getPrice(Integer val) {
        int a = val % 10;
        val /= 10;
        int b = val % 10;
        val /= 10;
        return val + "." + b + a;
    }

    private String getName(String s) {
        int n = s.length();
        int i = 0;
        while (i < s.length() && !Character.isLetter(s.charAt(i)))
            ++i;
        StringBuilder t = new StringBuilder();
        for (int j = i; j < n; ++j) {
            t.append(s.charAt(j));
        }
        t = new StringBuilder(t.toString().replaceAll("\n", ""));
        t = new StringBuilder(t.toString().replaceAll(" {4}", " "));
        return t.toString();
    }
}