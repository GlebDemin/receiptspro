package com.samsung.receiptspro.receipts;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.samsung.receiptspro.R;
import com.samsung.receiptspro.network.models.ReceiveResult;
import com.samsung.receiptspro.receipts.adapters.ProductsAdapter;

import java.util.Objects;

public class ReceiptDisplayActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProductsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiptdisplay);

        recyclerView = findViewById(R.id.products_view);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Bundle arguments = getIntent().getExtras();
        final ReceiveResult receipt;

        if(arguments != null){
            receipt = (ReceiveResult) getIntent().getExtras().getSerializable("receipt");
            mAdapter = new ProductsAdapter(this, Objects.requireNonNull(receipt).getProducts());
            recyclerView.setAdapter(mAdapter);

        }

    }

}