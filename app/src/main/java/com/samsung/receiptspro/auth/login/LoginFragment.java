package com.samsung.receiptspro.auth.login;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.samsung.receiptspro.MainActivity;
import com.samsung.receiptspro.R;
import com.samsung.receiptspro.account.AccountManager;
import com.samsung.receiptspro.network.BaseRequestMaker;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.LoginResult;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {

    private LoginViewModel loginViewModel;
    private ProgressDialog mProgressDialog;
    private Server client;

    private AccountManager accountManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        loginViewModel =
                ViewModelProviders.of(this).get(LoginViewModel.class);

        this.accountManager = new AccountManager(
                requireActivity().getSharedPreferences(
                        AccountManager.getPrefsName(),
                        MODE_PRIVATE
                )
        );
        this.client = new BaseRequestMaker().getClient();

        this.mProgressDialog = new ProgressDialog(this.getContext());
        this.mProgressDialog.setTitle(getString(R.string.logging_in));
        this.mProgressDialog.setIndeterminate(false);
        this.mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.mProgressDialog.setCancelable(false);

        View root = inflater.inflate(R.layout.fragment_login, container, false);
        Button loginBtn = root.findViewById(R.id.btn_login);
        EditText etPhone = root.findViewById(R.id.et_mobile);
        EditText etPassword = root.findViewById(R.id.et_password);

        loginBtn.setOnClickListener(v -> {

            String phone = etPhone.getText().toString();
            String password = etPassword.getText().toString();

            Call<LoginResult> call = client.login(phone, password);

            this.mProgressDialog.show();

            call.enqueue(new Callback<LoginResult>() {
                @Override
                public void onResponse(@NonNull Call<LoginResult> call, @NonNull Response<LoginResult> response) {
                    if (response.isSuccessful()) {
                        loginSuccess(phone, Objects.requireNonNull(response.body()).getEmail(),
                                password, response.body().getName());
                    }
                    else {
                        loginFailure();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginResult> call, @NonNull Throwable t) {
                    loginError();
                }
            });
        });
        return root;
    }

    private void showToast(String text){
        Toast.makeText(
                this.getContext(),
                text,
                Toast.LENGTH_LONG
        ).show();
    }

    private void AfterResponse(String text){
        showToast(text);
        this.mProgressDialog.cancel();
    }

    private void loginSuccess(String phone, String email, String password, String username){
        AfterResponse(String.format("%s, %s!", getString(R.string.welcome), username));
        Intent intent = new Intent(this.getContext(), MainActivity.class);
        this.accountManager.login(phone, email, password);
        startActivity(intent);
        this.requireActivity().finish();
    }

    private void loginFailure(){
        AfterResponse(String.format("%s!", getString(R.string.user_not_found)));
    }

    private void loginError(){
        AfterResponse(String.format("%s!", getString(R.string.connection_error)));
    }

}