package com.samsung.receiptspro.auth.restore;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.samsung.receiptspro.R;
import com.samsung.receiptspro.network.BaseRequestMaker;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.RestoreResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestoreFragment extends Fragment {

    private RestoreViewModel restoreViewModel;

    private ProgressDialog mProgressDialog;
    private Server client;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        restoreViewModel =
                ViewModelProviders.of(this).get(RestoreViewModel.class);

        this.client = new BaseRequestMaker().getClient();

        this.mProgressDialog = new ProgressDialog(this.getContext());
        this.mProgressDialog.setTitle(getString(R.string.restoring));
        this.mProgressDialog.setIndeterminate(false);
        this.mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.mProgressDialog.setCancelable(false);

        View root = inflater.inflate(R.layout.fragment_restore, container, false);
        Button restoreBtn = root.findViewById(R.id.btn_restore);
        EditText etPhone = root.findViewById(R.id.et_mobile);

        restoreBtn.setOnClickListener(v -> {

            String phone = etPhone.getText().toString();

            Call<RestoreResult> call = client.restore(phone);

            this.mProgressDialog.show();

            call.enqueue(new Callback<RestoreResult>() {
                @Override
                public void onResponse(Call<RestoreResult> call, Response<RestoreResult> response) {
                    if (response.isSuccessful()) {
                        Log.i("Info", "" + response.body().getResult());
                        restoreSuccess();
                    }
                    else {
                        restoreFailure();
                    }

                    Log.i("Info", "" + response.code());
                    Log.i("Info", "" + response.message());
                }

                @Override
                public void onFailure(Call<RestoreResult> call, Throwable t) {
                    restoreError();
                }
            });

        });

        return root;
    }

    private void showToast(String text){
        Toast.makeText(
                this.getContext(),
                text,
                Toast.LENGTH_LONG
        ).show();
    }

    private void AfterResponse(String text){
        showToast(text);
        this.mProgressDialog.cancel();
    }

    private void restoreSuccess(){
        AfterResponse(String.format("%s", getString(R.string.restore_success)));
    }

    private void restoreFailure(){
        AfterResponse(String.format("%s!", getString(R.string.restore_failure)));
    }

    private void restoreError(){
        AfterResponse(String.format("%s!", getString(R.string.connection_error)));
    }
}
