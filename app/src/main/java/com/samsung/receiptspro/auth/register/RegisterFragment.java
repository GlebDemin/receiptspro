package com.samsung.receiptspro.auth.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.samsung.receiptspro.R;
import com.samsung.receiptspro.network.BaseRequestMaker;
import com.samsung.receiptspro.network.Server;
import com.samsung.receiptspro.network.models.RegistrationResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    private RegisterViewModel registerViewModel;

    private ProgressDialog mProgressDialog;
    private Server client;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        registerViewModel =
                ViewModelProviders.of(this).get(RegisterViewModel.class);

        this.client = new BaseRequestMaker().getClient();

        this.mProgressDialog = new ProgressDialog(this.getContext());
        this.mProgressDialog.setTitle(getString(R.string.registering));
        this.mProgressDialog.setIndeterminate(false);
        this.mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.mProgressDialog.setCancelable(false);

        View root = inflater.inflate(R.layout.fragment_register, container, false);
        Button registerBtn = root.findViewById(R.id.btn_register);
        EditText etName = root.findViewById(R.id.et_name);
        EditText etPhone = root.findViewById(R.id.et_mobile);
        EditText etEmail = root.findViewById(R.id.et_email);
        CheckBox cbPrivacy = root.findViewById(R.id.check_privacy);
        TextView tvPrivacy = root.findViewById(R.id.privacy_text);

//        cbPrivacy.setOnCheckedChangeListener((buttonView, isChecked) -> registerBtn.setEnabled(isChecked));
        tvPrivacy.setText(Html.fromHtml(
                getString(R.string.privacy_begin) +
                        "<a href='" + BaseRequestMaker.URL +
                        "policy'>" + getString(R.string.privacy_end) + "</a>"));
        tvPrivacy.setMovementMethod(LinkMovementMethod.getInstance());

        registerBtn.setOnClickListener(v -> {

            if(!cbPrivacy.isChecked()){
                privacyFailure();
                return;
            }

            String name = etName.getText().toString();
            String phone = etPhone.getText().toString();
            String email = etEmail.getText().toString();

            Call<RegistrationResult> call = client.registration(name, phone, email);

            this.mProgressDialog.show();

            call.enqueue(new Callback<RegistrationResult>() {
                @Override
                public void onResponse(Call<RegistrationResult> call, Response<RegistrationResult> response) {
                    if (response.isSuccessful()) {
                        registerSuccess();
                    }
                    else {
                        registerFailure();
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResult> call, Throwable t) {
                    registerError();
                }
            });

        });

        return root;
    }

    private void showToast(String text){
        Toast.makeText(
                this.getContext(),
                text,
                Toast.LENGTH_LONG
        ).show();
    }

    private void AfterResponse(String text){
        showToast(text);
        this.mProgressDialog.cancel();
    }

    private void registerSuccess(){
        AfterResponse(String.format("%s", getString(R.string.register_success)));
    }

    private void registerFailure(){
        AfterResponse(String.format("%s!", getString(R.string.user_exists)));
    }

    private void registerError(){
        AfterResponse(String.format("%s!", getString(R.string.connection_error)));
    }

    private void privacyFailure(){
        Snackbar.make(requireView(), getString(R.string.privacy_failure), Snackbar.LENGTH_LONG).show();
    }
}