package com.samsung.receiptspro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.samsung.receiptspro.account.AccountManager;
import com.samsung.receiptspro.auth.AuthActivity;


public class SplashActivity extends AppCompatActivity {

    private AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.accountManager = new AccountManager(
                getSharedPreferences(
                        AccountManager.getPrefsName(),
                        Context.MODE_PRIVATE
                )
        );

        if(this.accountManager.isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(this, AuthActivity.class);
            startActivity(intent);
        }
        finish();
    }

}
