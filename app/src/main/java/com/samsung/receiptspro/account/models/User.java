package com.samsung.receiptspro.account.models;

import androidx.annotation.NonNull;

public class User {

    private String phone;
    private String email;
    private String password;

    public User(String phone, String email, String password){
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getEmail() {
        return this.email;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("User<phone: %s, password: %s>", this.phone, this.password);
    }
}
