package com.samsung.receiptspro.account;

import android.content.SharedPreferences;

import com.samsung.receiptspro.account.models.User;

public class AccountManager {

    private static final String PREFS_NAME = "accountDetails";
    private static final String PHONE = "phone";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String IS_LOGIN_IN = "isLoginIn";

    private SharedPreferences userPrefs;

    public AccountManager(SharedPreferences userPrefs){
        this.userPrefs = userPrefs;
    }

    public static String getPrefsName(){
        return PREFS_NAME;
    }

    public boolean isLoggedIn(){
        return this.userPrefs.getBoolean("isLoginIn", false);
    }

    public void login(String phone, String email, String password){
        SharedPreferences.Editor prefEditor = this.userPrefs.edit();
        prefEditor.putString(PHONE, phone);
        prefEditor.putString(EMAIL, email);
        prefEditor.putString(PASSWORD, password);
        prefEditor.putBoolean(IS_LOGIN_IN, true);
        prefEditor.apply();
    }

    public void logout(){
        SharedPreferences.Editor prefEditor = this.userPrefs.edit();
        prefEditor.clear();
        prefEditor.apply();
    }

    public User getCurrentUser(){
        return new User(
                this.userPrefs.getString(PHONE, ""),
                this.userPrefs.getString(EMAIL, ""),
                this.userPrefs.getString(PASSWORD, "")
        );
    }
}
