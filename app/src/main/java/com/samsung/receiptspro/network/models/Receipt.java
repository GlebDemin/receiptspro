package com.samsung.receiptspro.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Receipt {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("receipt")
    @Expose
    private Receipt_ receipt;

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Receipt withDate(String date) {
        this.date = date;
        return this;
    }

    public Receipt_ getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt_ receipt) {
        this.receipt = receipt;
    }

    public Receipt withReceipt(Receipt_ receipt) {
        this.receipt = receipt;
        return this;
    }

}