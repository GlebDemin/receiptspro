package com.samsung.receiptspro.network.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "dateTime",
        "products",
        "totalSum"
})
public class ReceiveResult implements Serializable {

    @JsonProperty("dateTime")
    private String dateTime;
    @JsonProperty("products")
    private List<Product> products;
    @JsonProperty("totalSum")
    private Integer totalSum;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ReceiveResult(Receipt_ receipt){
        dateTime = receipt.getDateTime();
        products = receipt.getProducts();
        totalSum = receipt.getTotalSum();
    }

    @JsonProperty("dateTime")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("dateTime")
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @JsonProperty("totalSum")
    public Integer getTotalSum() {
        return totalSum;
    }

    @JsonProperty("totalSum")
    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}