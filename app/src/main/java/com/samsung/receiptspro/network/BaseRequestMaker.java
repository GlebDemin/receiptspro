package com.samsung.receiptspro.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRequestMaker {

    public static String URL = "https://receiptspro.herokuapp.com/";

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private Server client = retrofit.create(Server.class);

    public Server getClient(){
        return this.client;
    }
}