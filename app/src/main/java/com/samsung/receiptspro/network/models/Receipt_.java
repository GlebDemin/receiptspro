package com.samsung.receiptspro.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Receipt_ {

    @SerializedName("dateTime")
    @Expose
    private String dateTime;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("totalSum")
    @Expose
    private Integer totalSum;

    private static DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    private static DateFormat outputDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

    public String getDateTime() {
        Date nowDate;

        try{
            nowDate = inputDateFormat.parse(dateTime);
        }
        catch (ParseException e){
            return dateTime;
        }

        if (nowDate == null){
            return dateTime;
        }
        return outputDateFormat.format(nowDate);
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Receipt_ withDateTime(String dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Receipt_ withProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    Integer getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
    }

    public Receipt_ withTotalSum(Integer totalSum) {
        this.totalSum = totalSum;
        return this;
    }

}