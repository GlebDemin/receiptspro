package com.samsung.receiptspro.network;

import com.samsung.receiptspro.network.models.LoginResult;
import com.samsung.receiptspro.network.models.ReceiptsList;
import com.samsung.receiptspro.network.models.ReceiveResult;
import com.samsung.receiptspro.network.models.RegistrationResult;
import com.samsung.receiptspro.network.models.RestoreResult;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Server {
    @FormUrlEncoded
    @POST("client/login/")
    Call<LoginResult> login(
            @Field("phone") String phone,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("client/registration/")
    Call<RegistrationResult> registration(
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("client/restore/")
    Call<RestoreResult> restore(
            @Field("phone") String phone
    );

    @FormUrlEncoded
    @POST("client/receive/")
    Call<ReceiveResult> receive(
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("scan_result") String scan_result
    );

    @GET("api/v2/receipts")
    Call<ReceiptsList> getReceiptsRange(
            @Query("phone") String phone,
            @Query("period") String period
    );
}