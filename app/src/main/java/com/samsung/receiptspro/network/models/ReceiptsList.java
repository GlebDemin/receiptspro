package com.samsung.receiptspro.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceiptsList {

    @SerializedName("receipts")
    @Expose
    private List<Receipt> receipts = null;

    public List<Receipt> getReceipts() {
        return receipts;
    }

    public void setReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
    }

    public ReceiptsList withReceipts(List<Receipt> receipts) {
        this.receipts = receipts;
        return this;
    }

}